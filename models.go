package usermanager

import (
	"github.com/jinzhu/gorm"
)

type ApiLogin struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

type ApiReqRegister struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
	Email 	 string `form:"email" json:"email" binding:"required"`
}

type ApiResponse struct {
	Message string `json:"msg"`
}

type User struct {
	gorm.Model

	Username     string `gorm:"type:varchar(50);unique_index"`
	PasswordHash string `gorm:"type:varchar(100);not null"`
	Email		 string `gorm:"type:varchar(255);unique_index"`
}