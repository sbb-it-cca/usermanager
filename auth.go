package usermanager

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/dgrijalva/jwt-go.v3"
)

type AuthMiddleware struct {
	realm            string
	key              []byte
	signingAlgorithm *jwt.SigningMethodHMAC
	timeout          time.Duration
	userManager      *UserManager
}

func (am *AuthMiddleware) LoginHandler(c *gin.Context) {
	var loginData ApiLogin

	if c.BindJSON(&loginData) != nil {
		c.JSON(http.StatusBadRequest, ApiResponse{Message: "Missing Username or Password"})
		return
	}

	loginIsError := false

	user, err := am.userManager.Get(loginData.Username)
	if err != nil || user == nil {
		loginIsError = true
		log.Warn().Msgf("Login error: %s", err)
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.PasswordHash), []byte(loginData.Password))
	if err != nil {
		loginIsError = true
	}

	if loginIsError {
		c.Header("WWW-Authenticate", "JWT realm="+am.realm)
		c.JSON(http.StatusUnauthorized, ApiResponse{Message: "Wrong Username or Password"})
		return
	}

	// Create the token
	token := jwt.New(am.signingAlgorithm)
	claims := token.Claims.(jwt.MapClaims)

	expire := time.Now().Add(am.timeout)
	claims["id"] = user.Username
	claims["exp"] = expire.Unix()
	claims["orig_iat"] = time.Now().Unix()

	tokenString, err := token.SignedString(am.key)
	if err != nil {
		c.Header("WWW-Authenticate", "JWT realm="+am.realm)
		c.JSON(http.StatusUnauthorized, ApiResponse{Message: "Creating JWT token failed!"})
		log.Warn().Msgf("Error signing token: %s", err.Error())
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"token":  tokenString,
		"expire": expire.Format(time.RFC3339),
	})
}

func (am *AuthMiddleware) MiddlewareFunc() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Get token from the Authorization header if available
		// format: Authorization: Bearer <token>
		tokenString := c.GetHeader("Authorization")
		if len(tokenString) >= 1 {
			tokenString = strings.TrimPrefix(tokenString, "Bearer ")

			token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
				// Check algorithm
				if am.signingAlgorithm != token.Method {
					return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
				}

				return am.key, nil
			})

			if err != nil {
				c.Header("WWW-Authenticate", "JWT realm="+am.realm)
				c.AbortWithStatusJSON(http.StatusUnauthorized, ApiResponse{Message: err.Error()})
				return
			}

			claims := token.Claims.(jwt.MapClaims)
			userId := claims["id"].(string)

			// check if user exits
			user, err := am.userManager.Get(userId)
			if err != nil {
				c.Header("WWW-Authenticate", "JWT realm="+am.realm)
				c.AbortWithStatusJSON(http.StatusUnauthorized, ApiResponse{Message: err.Error()})
				return
			}

			// set user
			c.Set("user", user)

			// go further
			c.Next()
			return
		}

		c.Header("WWW-Authenticate", "JWT realm="+am.realm)
		c.AbortWithStatusJSON(http.StatusUnauthorized, ApiResponse{Message: "You need to be logged in to perform this action."})
	}
}