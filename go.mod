module gitlab.com/sbb-it-cca/usermanager

go 1.12

require (
	github.com/badoux/checkmail v0.0.0-20181210160741-9661bd69e9ad // indirect
	github.com/gin-contrib/sse v0.0.0-20190301062529-5545eab6dad3 // indirect
	github.com/gin-gonic/gin v1.3.0
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/jinzhu/gorm v1.9.2
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/rs/zerolog v1.13.0
	github.com/ugorji/go v1.1.4 // indirect
	golang.org/x/crypto v0.0.0-20190404164418-38d8ce5564a5
	gopkg.in/dgrijalva/jwt-go.v3 v3.2.0
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
