package usermanager

import (
	"github.com/jinzhu/gorm"
	"github.com/rs/zerolog/log"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/dgrijalva/jwt-go.v3"
	"time"
)

const (
	DEFAULT_EMAIL    = "admin@example.org"
	DEFAULT_USERNAME = "admin"
	DEFAULT_PASSWORD = "admin"
	BCRYPT_COST      = 8
)

type UserManager struct {
	db *gorm.DB
}

func (um *UserManager) Create(username string, email string, password string) error {
	user := User{
		Username: username,
		Email: email,
	}

	passwordHash, err := bcrypt.GenerateFromPassword([]byte(password), BCRYPT_COST)
	if err != nil {
		return err
	}

	user.PasswordHash = string(passwordHash)

	return um.db.Create(&user).Error
}

func (um *UserManager) Get(username string) (*User, error) {
	var user User

	err := um.db.Where("username = ?", username).Take(&user).Error
	return &user, err
}

func (um *UserManager) GetByEmail(email string) (*User, error) {
	var user User

	err := um.db.Where("email = ?", email).Take(&user).Error
	return &user, err
}

func (um *UserManager) GetAuthMiddleware(realm string, sessionKey string) *AuthMiddleware {
	return &AuthMiddleware{
		realm:            realm,
		key:              []byte(sessionKey),
		signingAlgorithm: jwt.SigningMethodHS512,
		timeout:          time.Hour * 24,
		userManager:      um,
	}
}

func (um *UserManager) GetGinHandler() *GinHandler {
	return &GinHandler{
		userManager: um,
	}
}

func New(db *gorm.DB) *UserManager {
	um := &UserManager{
		db: db,
	}

	if !db.HasTable(&User{}) {
		db.AutoMigrate(&User{})

		initUserTable(um)
	} else {
		db.AutoMigrate(&User{})
	}

	return um
}

func initUserTable(um *UserManager) {
	log.Info().Msg("Creating default user...")
	err := um.Create(DEFAULT_USERNAME, DEFAULT_EMAIL, DEFAULT_PASSWORD)
	if err != nil {
		log.Error().Msgf("Error creating default user: %s", err.Error())
	}
}
