package usermanager

import (
	"fmt"
	"github.com/badoux/checkmail"
	"github.com/jinzhu/gorm"
	"net/http"
	"regexp"

	"github.com/gin-gonic/gin"
)

const (
	// Success messages
	MsgSucUserCreated = "User '%s' has been created!"

	// Error messages
	MsgErrMissingFields = "Missing fields (needed username, password, email)."
	MsgErrCreateUser = "Error creating user: %s"
)

type GinHandler struct {
	userManager      *UserManager
}

func (gh *GinHandler) RegisterHandler(c *gin.Context) {
	// register data
	var registerData ApiReqRegister
	if c.BindJSON(&registerData) != nil {
		c.JSON(http.StatusBadRequest, apiResponse(MsgErrMissingFields))
		return
	}

	// validate data
	if valid, err := gh.validateNewUser(registerData); !valid {
		c.JSON(http.StatusBadRequest, apiResponse(err))
		return
	}

	// create user
	if err := gh.userManager.Create(registerData.Username, registerData.Email, registerData.Password); err != nil {
		c.JSON(http.StatusInternalServerError, apiResponse(fmt.Sprintf(MsgErrCreateUser, err.Error())))
		return
	}

	c.JSON(http.StatusCreated, apiResponse(fmt.Sprintf(MsgSucUserCreated, registerData.Username)))
}

func (gh *GinHandler) validateNewUser(user ApiReqRegister) (bool, string) {
	// username checks
	if len(user.Username) < 3 {
		return false, "Username must have at least 3 chars."
	}

	if len(user.Username) > 50 {
		return false, "Username can have at maximum 50 chars."
	}

	if _, err := gh.userManager.Get(user.Username); err != gorm.ErrRecordNotFound {
		return false, "Username is already registered."
	}

	// email checks
	if err := checkmail.ValidateFormat(user.Email); err != nil {
		return false, "Email address is not valid."
	}

	if len(user.Email) > 255 {
		return false, "Email address can have at maximum 255 chars."
	}

	if _, err := gh.userManager.GetByEmail(user.Email); err != gorm.ErrRecordNotFound {
		return false, "Email address is already registered."
	}

	// password checks
	if len(user.Password) < 8 {
		return false, "Password must have at least 8 chars."
	}

	if !gh.validatePassword(user.Password) {
		return false, "Password must have at least one letter, one number and one special character."
	}

	return true, "";
}

func (gh *GinHandler) validatePassword(password string) bool {
	letter, err := regexp.Compile("[a-zA-Z]")
	if err != nil {
		return false;
	}

	number, err := regexp.Compile("[0-9]")
	if err != nil {
		return false;
	}

	special, err := regexp.Compile("[^A-Za-z0-9]")
	if err != nil {
		return false;
	}

	return letter.MatchString(password) && number.MatchString(password) && special.MatchString(password)
}

func apiResponse(message string) ApiResponse {
	return ApiResponse{Message: message}
}
